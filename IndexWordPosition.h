#pragma once

#include <string>
using namespace std;

#include "preprocessor.h"

struct IndexWordPosition {

	IndexWordPosition* next;
	size_t id;

	IndexWordPosition(size_t &);

	IRString serialize() {
		IRString res = IRString("");

		IndexWordPosition* cur = this;

		while (true) {
			if (cur != NULL) {
				res.append(id+",");
				cur = cur->next;
			} else {
				break;
			}
		}
		return res;

	}
};

