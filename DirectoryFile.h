#pragma once

#include <iostream>
#include <string>
using namespace std;

#include "preprocessor.h"

struct DirectoryFile {

	DirectoryFile* next; 
	string fileName;
	string* body;

	DirectoryFile(string &filename):fileName(filename),next(NULL),body(NULL) {}

};


