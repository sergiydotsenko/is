#pragma once

#include "dirent.h"
#include "DirectoryFile.h" 

class DirectoryHandler {

	private:
		DirectoryFile* startFiles;
		size_t _count;

	public:

		DirectoryHandler(char* dirpath);
		~DirectoryHandler();
		DirectoryFile* files();
		size_t count();
};


