#pragma once

#include <fstream>
#include <map>
using namespace std;

#include "preprocessor.h"
#include "InvertedIndexWordBooks.h"
#include "ListHandler.h"

template <typename adressLen>
class StructuredFile {

	struct BookIdsOfsets {
		adressLen id;
		BookIdsOfsets* next;
		BookIdsOfsets(adressLen a):id(a) {}
	};

	fstream* _file;

	adressLen _curPos;
	map<adressLen,adressLen> ofsetsMap;

	BookIdsOfsets* ofsetsList;
	ListHandler<BookIdsOfsets,long>* hList;


public:
	StructuredFile(string& filedir):
		_file(new fstream()),
		ofsetsList(NULL),
		ofsetsMap(map<adressLen,adressLen>()),
		_curPos(0)
	{
		_file->open(filedir,ios::binary|ios::out|ios::in);
		hList = new ListHandler<BookIdsOfsets,long>(ofsetsList);
	}

	~StructuredFile(void) {
		this->_file->close();	
		delete this->_file;
	}

	/** another class extends of this */
	// wordId, bookId
	string readSpatialList(adressLen &wId,adressLen &bId) {
		adressLen offset = this->ofsetsMap.find(wId)->second;
		int sWId = read<int>(offset);
		bool found = false;

		while(_file->tellg() != _len && wId != sWId) {
			int cWordDataOffset = read<int>(_curPos);
			_curPos += cWordDataOffset;
		}
		if (found) {
			int bookId = read<int>(offset+sizeof(int)); 

			while(_file->tellg() != _len && wId != sWId) {
				int cWordDataOffset = read<int>(_curPos);
				_curPos += cWordDataOffset;
			}	
			adressLen size = read<adressLen>(_curPos);
			//string spatialIndex = ;
			return read<string>(_curPos,size);
		}
	}

	adressLen writeBookList(InvertedIndexWordBooks* pB) {
		DEBUG("file open ? " << this->_file->is_open());
		DEBUG("WRITING TO DISK");
		if (pB) {
			write<size_t>(pB->id,_curPos);
			IRString spInd = pB->pos->serialize();

			adressLen sLen = spInd.size();

			write<adressLen>(sLen,_curPos);
			write(spInd,_curPos);
			if (pB->next) {
				return spInd.size()+sizeof(adressLen)+sizeof(size_t) + writeBookList(pB->next);
			} else {
				return spInd.size()+sizeof(adressLen)+sizeof(size_t);
			}
		} else {
			return 0;
		}
	}

private:
	template<typename T> T read(adressLen &offset){
		return this->read(offset,sizeof(T));
	}
	template<typename T> T read(adressLen &offset,adressLen &len) {
		T res = 0;
		_file->sekg(0,offset);
		_file->read((char*)res,len);
		_curPos = _file->tellg();
		return res;
	}

	template<typename T> void write(T &elem,adressLen &pos) {
		adressLen size = sizeof(T);
		this->_write<T>(elem,pos,size);
	}

	void write(string &elem,adressLen &pos) {
		_file->seekg(pos);
		_file->write((char*)elem.c_str(),elem.size());
		_curPos += elem.size(); 
	}

	template<typename T> void _write(T &elem,adressLen &pos,adressLen len) {
		_file->seekg(pos);
		_file->write((char*)elem,len);
		_curPos += len; 
	}





};

