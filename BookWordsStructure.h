#pragma once

#include "preprocessor.h"
#include "BookWordsItemStructure.h"

struct BookWords {

	BookWordsItem* start;
	unsigned int length;

	unsigned int bookId;

	BookWords():start(NULL),length(0){}

	~BookWords() {
		#ifdef RELEASE_BOOK_WORDS_TOKENS 
			BookWordsItem* cur = this->start;
			BookWordsItem* prev;

			while(cur != NULL) {
				prev = cur; cur = cur->next;
				delete prev;
			}
		#endif /*RELEASE_BOOK_WORDS_TOKENS*/
	}

};
