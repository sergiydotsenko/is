#include <iostream>
#include <ctime>
#include <string>
#include <windows.h>
using namespace std;

#include "preprocessor.h"
#include "IndexHandler.h"
#include "InvertedIndex.h"
#include "InvertedIndexWord.h"
#include "TokenizerInterface.h"
#include "Tokenizers/Fb2Tokenizer.cpp"
#include "DirectoryHandler.h"
#include "SearchUserInterface.h"
//#include "ModifiedString.h"


// heap debug 
#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>


void main() {
	
	// setting up ukrainian encodings 
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	setlocale(LC_ALL, "Ukrainian");

	InvertedIndex* ii = new InvertedIndex(new InvertedIndexWord());
	IndexHandler hIndex = IndexHandler(ii);

	char* dirToProceed = "C:/Users/Serho/Documents/Visual Studio 2012/Projects/infosearch/Release/files";
	DirectoryHandler* hDir = new DirectoryHandler(dirToProceed);

	TokenizerInterface* tokenizer;
	if (hDir->count()) {
		DirectoryFile*  cur = hDir->files();

		int bookId = 0;
		while (cur != NULL) {
			clock_t begin = clock();

			tokenizer = new Fb2Tokenizer(dirToProceed+ IRString("/") + cur->fileName.c_str());	
			bookId++;
			hIndex.processBook(&tokenizer->tokens(),bookId);

			DEBUG(" processed " << cur->fileName.c_str()<< " ["<< double(clock() - begin) / CLOCKS_PER_SEC<<"s.]");

			cur = cur->next;

			// memory release 
			delete tokenizer;
		}

	}
	
	//memory release
	delete hDir;

	//hIndex.dump("db/words/1.iswl");



	SearchUserInterface ui = SearchUserInterface(&hIndex);
	ui.prompt();
	
	// memory release
	delete ii;

	
	_CrtDumpMemoryLeaks();
}