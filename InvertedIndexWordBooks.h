#pragma once

#include <iostream>

#include "preprocessor.h"
#include "IndexWordPosition.h"

struct InvertedIndexWordBooks
{
	InvertedIndexWordBooks* next;
	IndexWordPosition* pos;
	
	size_t id;
	//size_t count;	
	explicit InvertedIndexWordBooks();
	explicit InvertedIndexWordBooks(const size_t &,IndexWordPosition* &);
	~InvertedIndexWordBooks();


	
	// returns pointer to a book structure
	InvertedIndexWordBooks* containBook(size_t bookId) {
		// only if list sorted ascendantly
		if (this->id > bookId) {
			return NULL;
		}

		InvertedIndexWordBooks* c = this;
		if (c->id == bookId) {
			return c;
		} c = c->next;

		while(c != NULL) {
			if (c->id == bookId) {
				return c;
			}
			c = c->next;
		}
		return NULL;

	}
	
};