#pragma once

#include <iostream>
#include <fstream>
using namespace std;

#include "preprocessor.h"
#include "InvertedIndexWord.h"
#include "IndexWordPosition.h"
#include "BookWordsItemStructure.h"


template <typename T> 
void pushElement(T* &start,T* &e) {

	if (start == NULL) {
		start = e;
		return;
	}

	T* tmp;
	if (e->id <= start->id) {
		tmp = start;
		start = e;
		e->next = tmp;
	} else {
		tmp = start;
		T* prev;
		while (tmp != NULL && tmp->id <= e->id) {
			prev = tmp;
			tmp = tmp->next;
		}
		if (tmp != NULL) {
			e->next = tmp;
		}
		prev->next = e;
	}
}

class InvertedIndex {

	InvertedIndexWord* root;
	ofstream contentsFile;
	IRString currentChar;


public :
	InvertedIndex(InvertedIndexWord* root):
		root(root),
		currentChar(IRString(""))
	{
	}
	~InvertedIndex(){
		#ifdef RELEASE_INVERTED_INDEX 
			this->releaseBTreeItem(this->root);
		#endif
	}

#ifdef RELEASE_INVERTED_INDEX

private:
	void releaseBTreeItem(InvertedIndexWord* l){

		// go deeper
		if (l->next->size() > 0) {
			for( auto it = l->next->begin(); it != l->next->end(); ++it) {
				this->releaseBTreeItem(it->second);
			}			
		}

		// release books list
		if (l->bookList != NULL) {
			this->releaseWB(l->bookList);
		}
		delete l->next;
		delete l;
	}

	void releaseWB(InvertedIndexWordBooks* bl) {
			
			// go deeper
			if (bl->next != NULL)  {
				this->releaseWB(bl->next);
			}
			//release positions 
			if (bl->pos != NULL) { 
				this->releaseIWP(bl->pos);
			}
			
			//delete bl->lastPos;
			delete bl;
	}

	void releaseIWP(IndexWordPosition* wp) {
		
			if (wp->next != NULL) {
				this->releaseIWP(wp->next);
			}

			delete wp; 

	}

public:

#endif

	InvertedIndexWord* const getRoot() {
		// right, because at the top always an empty element, 
		// so next will be grater and will be in the right branch
		return this->root;
	}

	void addWord(BookWordsItem* const  &word,int &bookId) {
		InvertedIndexWord* cur = this->root;
		size_t wActualSize = word->value.length()-1;

		for (size_t k = 0;k < wActualSize+1; k++) {
			char i = word->value[k];
			if (cur->next->find(i) != cur->next->end()) { // if find char in next's 
				cur = cur->next->find(i)->second; // set next char as cur 
			} else { // insert new char 
				InvertedIndexWord* tmp = new InvertedIndexWord();
				cur->next->insert(pair<char,InvertedIndexWord*>(i,tmp));
				cur = tmp;
			}

			if (k == wActualSize) {
				IndexWordPosition* iwp = new IndexWordPosition(word->posInBook);
				InvertedIndexWordBooks* iiw = new InvertedIndexWordBooks(bookId,iwp);

				// if already in a index
				if(cur->isLast == true) {
					cur->qty++; 

					InvertedIndexWordBooks* bAddr = cur->bookList->containBook(bookId);
					if (bAddr != NULL) {
						pushElement<IndexWordPosition>(bAddr->pos,iwp);
						delete iiw;
					} else {
						pushElement<InvertedIndexWordBooks>(cur->bookList,iiw);
					}
				} else {
					cur->isLast = true;
					pushElement<InvertedIndexWordBooks>(cur->bookList,iiw);			
				}
			}
		}
	}

	void ˝ontentsToFile(string file) {
		this->contentsFile.open(file);
		this->printRoot(this->root);
		this->contentsFile.close();
	}


private :
	void printRoot(InvertedIndexWord* &elem) {
	
		string wStack = "";
		for (auto it=elem->next->begin(); it!=elem->next->end(); ++it) {
			//wStack.<< it->first ;
			/*if (it->second->next != NULL) {
		
			}*/
			if (it->second->isLast == true) {
				cout << "\n";
			}		
			this->printRoot(it->second);
		}
	}
	
};