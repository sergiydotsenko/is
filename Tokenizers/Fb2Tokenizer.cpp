#pragma once 

#include <iostream>
#include <string>
#include <regex>
#include <thread>
using namespace std;

#include "../TokenizerInterface.h"
#include "../BookWordsStructure.h"
#include "../preprocessor.h"
#include "../ModifiedString.h"


class Fb2Tokenizer: public  TokenizerInterface {

public:
	
	Fb2Tokenizer(IRString &filename):TokenizerInterface(filename){}

	BookWords& tokens() {	
		if (this->countWords <= 0) {
			
			int linesInBlock = 50000;
			this->readFileContents(linesInBlock,Fb2Tokenizer::multithread);
		} 
		return *this->words;
	}



	void static multithread(string& input,BookWordsItem* &bw,int lastWPosition)  {
		// remove tags

		regex reg_annotation("<annotation>(.*)</annotation>");

		smatch annot;
		regex_search(input, annot, reg_annotation);

		//cout <<"ANNOTATION:" <<  annot[0] << ";\n";

		regex tags("(<.*?>|-)");
		string temp  =  regex_replace(input, tags, "");

		// remove non alphabets
		regex nonLiterals("[^��������賿�����������������������å�Ū��Ȳ���������������������0-9-]");
		temp  =  regex_replace(temp,nonLiterals," ");

		// explode array 
		//recursive_mutex _mutex;

		istringstream iss(temp);    

		BookWordsItem* current = NULL;
		int i = 0;

		string token; 
		
		if (bw == NULL) {
			getline(iss, token,' ');
			lastWPosition++;
			bw = new BookWordsItem(token,lastWPosition);
		} 

		current = bw;

		for (string token; getline(iss, token, ' '); ) {
				
				// tokens to lower case
				transform(token.begin(), token.end(), token.begin(), ::tolower);

				current->next = new BookWordsItem(token,lastWPosition);
				current = current->next;
			i++;
		}

		
		
	}

};