#pragma once

/* OWN KEYS */

// release memory while InvertedIndex destructor called
#define RELEASE_INVERTED_INDEX

// release a memory from tokens after reading books
#define RELEASE_BOOK_WORDS_TOKENS

// release an info about files in directory
#define RELEASE_DIRECTORY_DATA

#define MODIFIED_STRING_VARIAL_CAPACITY

/* OWN KEYS END */

#define _DEBUG_
#define _CRT_SECURE_NO_WARNINGS

#define IRString string



#if defined _DEBUG_
#define DEBUG(out) cout << "-"<< out << endl ;
#else
#define DEBUG(out) 
#endif 



