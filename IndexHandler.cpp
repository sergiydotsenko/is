#include <string>
#include <sstream>
#include <windows.h>
#include <ctime>
using namespace std;

#include "preprocessor.h"
#include "generalFunctions.h"
#include "IndexHandler.h"

IndexHandler::IndexHandler(InvertedIndex* const &item):
	index(item),
	spatialSimilarityDistance(100),
	diskOperator(StructuredFile<unsigned int>(string("db/words/spatial.iswl")))
{
}


IndexHandler::~IndexHandler(void)
{
}

void IndexHandler::findBook(int id){
	// here connection with books list
}

void IndexHandler::executeQuery(string q) {
	this->analyzeQuery(q);
}

void showBWItem(InvertedIndexWordBooks* &item) {
	cout << "book item " << item->id<< endl;
}

void IndexHandler::analyzeQuery(string& q) {

	vector<string> _stack = vector<string>();

	istringstream iss(q);

	// push back first word of user query
	string token;

	for (string token; getline(iss, token, ' '); ) {
		_stack.push_back(token);
	}

	// print out analyzed search query
	vector<InvertedIndexWordBooks*> _bLists = vector<InvertedIndexWordBooks*>();

	bool queryEmptyResult = false;

	for (size_t i=0;i<_stack.size();i++) {
		//DEBUG("F`" << _stack[i]<<"`"); 

		InvertedIndexWordBooks* tmp = this->traverseContents(_stack[i]);
		if (tmp != NULL) {
		
			_bLists.push_back(tmp);
		
		} else {
		
			if (1) { // if its AND operand
				queryEmptyResult = true;
				break;
			}

		}
	}


	

	if (queryEmptyResult) {
		cout << "������, ���������� �� ������������ ��� ����� �� ��. ��������� ������ �����";
	} else {
		
		if (_stack.size() > 1) {
			InvertedIndexWordBooks* b1 = _bLists.back();_bLists.pop_back();
			InvertedIndexWordBooks* b2 = _bLists.back();_bLists.pop_back();

			
			DEBUG("WRITED : " << this->diskOperator.writeBookList(b1) << " bytes");
			this->diskOperator.writeBookList(b2);


			InvertedIndexWordBooks* _tmpMerge = this->mergeBookLists(b1,b2);

			//_tmpMerge->pos
			
			
			for (size_t k = 0; k < _bLists.size(); k++) { 
				_tmpMerge = this->mergeBookLists(_tmpMerge,_bLists[k]);
			}
			DEBUG("THE HOLY RESULT");
			traverseList<InvertedIndexWordBooks>(_tmpMerge,&showBWItem);

		} else { // only one word was in query
			traverseList<InvertedIndexWordBooks>(_bLists[0],&displayBookList);
		}
	
	}

}



InvertedIndexWordBooks* IndexHandler::checkRoot(InvertedIndexWord* const root,string& value)
{

	InvertedIndexWord* cur = this->index->getRoot();
	size_t wActualSize = value.length()-1;

	IRString stack = "";

	for (size_t k = 0;k < value.length(); k++) {
		char i = value[k];
		if (cur->next->find(i) != cur->next->end()) {
			cur = cur->next->find(i)->second;
			stack.push_back(i);
		} else {
			return NULL;
		}
		if (k == wActualSize) {
			if (cur->isLast && stack == value) {
				return cur->bookList;
			} else {
				return NULL;
			}
		} 
	}
	return NULL;
}


InvertedIndexWordBooks* IndexHandler::traverseContents(string& searchWord) {
	return checkRoot(this->index->getRoot(),searchWord);
}


InvertedIndexWordBooks* IndexHandler::mergeBookLists(InvertedIndexWordBooks* smaller,InvertedIndexWordBooks* bigger){
		
		InvertedIndexWordBooks *curSm, *curBig;
		InvertedIndexWordBooks *startBook, *nextMerged, *lastInList;

		startBook = new InvertedIndexWordBooks();

		lastInList = NULL;

		nextMerged = startBook;
		curSm = smaller;
		curBig = bigger;

		int length = 0;
		   
		while (curSm != NULL && curBig != NULL) {

			if (curSm->id == curBig->id) {
				if(this->mergeSpatialLists(curSm->pos,curBig->pos) > 0) {

					length++;
					// pushing an id of book to our stack

					lastInList = nextMerged;

					nextMerged->id = curSm->id;
				//	nextMerged->count = curSm->count;
					nextMerged->pos = curSm->pos;
					nextMerged->next = new InvertedIndexWordBooks();

					nextMerged = nextMerged->next;
				
					
				} 

				// assign next list items
				curSm = curSm->next;
				curBig = curBig->next;

			} else {
				if (curSm->id < curBig->id) {
					curSm = curSm->next;
				} else { 
					curBig = curBig->next;
				}
			}
		}

		if (length > 0 ) {
			lastInList->next = NULL;
			delete nextMerged;

			return startBook;  
		} else {

			//release unused memory
			delete startBook;
			return NULL;
		}
		
	}

int IndexHandler::mergeSpatialLists(IndexWordPosition* smaller,IndexWordPosition* bigger){
	return this->mergeSpatialLists(smaller,bigger,this->spatialSimilarityDistance);
}

int IndexHandler::mergeSpatialLists(IndexWordPosition* smaller,IndexWordPosition* bigger,short distance){

	IndexWordPosition *fList = smaller;
	IndexWordPosition *sList = bigger;

	int fOffset = 0; 
	int sOffset = 0;

	int length = 0;
		   
	while (fList != NULL && sList != NULL) {

		fOffset = fList->id;
		sOffset = sList->id;

		//DEBUG("["<<abs(fOffset-sOffset))
		if ((short)abs(fOffset-sOffset) <= distance) {
			return 1;

			fList = fList->next;
			sList = sList->next;

		} else {
			if (fOffset < sOffset) {
				fList = fList->next;
			} else { 
				sList = sList->next;
			}
		}
	}

	
	return length;


}

void IndexHandler::processBook(BookWords* const &book,int &bookId) {


	BookWordsItem* curW = book->start;

	// traverse
	while(curW != NULL) {
		if (curW->value > "a" ) {
			this->index->addWord(curW,bookId);
		}
		curW = curW->next;
	}

}

void IndexHandler::dump(string filename) {

	this->index->�ontentsToFile(filename);

}
