

/*#pragma once 

#include <iostream>
using namespace std;

#include "preprocessor.h"
#include "ModifiedString.h"

namespace IRString {
typedef short String_t;

class IRString {


	// here will be actual value consisted
	char* _val;

	// actual len
	String_t _len;

#ifdef MODIFIED_STRING_VARIAL_CAPACITY
	String_t _capacity;
#endif

public:

	// empty constructor
	IRString():
		_len(0),
		_val(new char[1]),
		_capacity(0)
	{
		_val[0] = '\0';
	}

	IRString(char* value):
		_len(strlen(value))
	{
		_val = new char[_len+1];
		strcpy(_val,value);
		_capacity = _len;
	}

	IRString(string& value):
		_len(value.size()),
		_val(new char[value.size()+1]),
		_capacity(value.size())
	{
		strcpy(this->_val,value.c_str());
	}

// constructor with allocation size param
#ifdef MODIFIED_STRING_VARIAL_CAPACITY	
	IRString(String_t capacity,char* val):
		_val(new char[capacity+1]),
		_capacity(capacity),
		_len(strlen(val))
	{
#ifdef _DEBUG_
		if (_len > _capacity) {
			throw exception("capacity less than init value!");
		}
#endif
		strcpy(this->_val,val);
	}
	IRString(String_t capacity):
		_val(new char[capacity+1]),
		_capacity(capacity),
		_len(0)
	{
		_val[0] = '\0';
	}

#endif

	~IRString() {
		//DEBUG("DELETTING `"<<_val<<"` :"<<&_val)
		//delete[] _val;
	}

	// compability to c++ std::string
	inline String_t size(){
		return this->_len;
	}

	inline String_t length() {
		return this->size();
	}

	IRString& push_back(char elem) {
		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			if (_capacity >= _len + 1) {
				_val[_len] = elem;
				_val[_len+1] = '\0';
				_len++;
			} else {
		#endif

			char* tmp = new char[_len+2]; // one for \0
			strncpy(tmp,_val,strlen(_val));
			delete [] _val;

			_val = tmp;
			_val[_len] = elem;
			_val[_len+1] = '\0';
			_capacity++;
			_len++;

		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			}
		#endif

		return *this;
	}

	const char& at(String_t point) {
		#ifdef _DEBUG_ 
			if (point > this->_len-1) {
				throw exception("Trying to get non exist element of string");
			}
		#endif
		return this->_val[point];
	}

	short compare(char* elem) {
		return strcmp (this->operator const char *(),elem);
	}

	short compare(IRString& elem) {
		return strcmp(this->c_str(),elem.c_str());
	}

	IRString& append(char* elem){
		return this->append(IRString(elem));
	}
	IRString& append(string& elem){
		if (elem.size() == 0) {
		return *this;
		}

		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			if (_capacity >= _len + elem.length()) { 
				if (_len > 0) {
					strcat(_val,elem.c_str());
				} else {
					strcpy(_val,elem.c_str());
				}
				
				_len += elem.length();
			} else {
		#endif
			char* tmp = new char[_len+(_capacity-_len)+elem.size()+1]; // one for \0
			if (_len > 0) {
				strcpy(tmp,_val);
				//delete [] _val;
			}
			_val = tmp;

			if (_len > 0) {
				strcat(_val,elem.c_str());
			} else {
				strcpy(_val,elem.c_str());
			}
			
			_len += elem.size();
			_capacity = _len;

		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			}
		#endif
		
		return *this;
	}

	IRString& append(IRString& elem){
		if (elem.size() == 0) {
		return *this;
		}

		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			if (_capacity >= _len + elem.length()) { 
				if (_len > 0) {
					strcat(_val,elem.c_str());
				} else {
					strcpy(_val,elem.c_str());
				}
				
				_len += elem.length();
			} else {
		#endif
			char* tmp = new char[_len+(_capacity-_len)+elem.size()+1]; // one for \0
			if (_len > 0) {
				strcpy(tmp,_val);
				//delete [] _val;
			}
			_val = tmp;

			if (_len > 0) {
				strcat(_val,elem.c_str());
			} else {
				strcpy(_val,elem.c_str());
			}
			
			_len += elem.size();
			_capacity = _len;

		#ifdef MODIFIED_STRING_VARIAL_CAPACITY
			}
		#endif
		
		return *this;
	}

	const char* c_str() {
#ifdef MODIFIED_STRING_VARIAL_CAPACITY
		return this->operator const char *();
#else 
		return this->_val;
#endif
	}


	operator const char* (){
#ifdef MODIFIED_STRING_VARIAL_CAPACITY
		//some actions
		return this->_val;
#else 
		return this->_val;
#endif
		
	}

	friend ostream& operator<< (ostream&,IRString&);
	friend IRString operator+ (char*,IRString&);
	friend IRString operator+ (IRString&,IRString&);
	friend IRString operator+ (IRString&,string&);

};
}
*/