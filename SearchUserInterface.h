#pragma once

#include "InvertedIndex.h"
#include "IndexHandler.h"

class SearchUserInterface {

	// index handler allocator
	IndexHandler* hIndex; 

	// maximal query length provided by user
	static const int QUERY_MAX_LEN;

public:
	SearchUserInterface(IndexHandler* const &item);
	~SearchUserInterface();
	void prompt();

};


