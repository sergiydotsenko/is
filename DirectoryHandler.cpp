#include "preprocessor.h"
#include "DirectoryHandler.h"


DirectoryHandler::DirectoryHandler(char* path):
	startFiles(NULL),
	_count(0)
{

	string c_content;
    DIR *c_dir = NULL;
	int endline = 0,length = 50;
    c_dir = opendir (path);
    struct dirent *contents = NULL;
    if (c_dir != NULL)
    {
		DirectoryFile* current = this->startFiles;
		while (contents = readdir (c_dir)) {
			if (contents != NULL) {
				string fileName = contents->d_name;

				if (!fileName.compare("..") == 0  && !fileName.compare(".") == 0) {
					string tmp = contents->d_name;
					if (!this->_count) {
						this->startFiles = current = new DirectoryFile(fileName);
						this->_count++;
					} else {
						current->next = new DirectoryFile(fileName);
						current = current->next;
						this->_count++;
					}
				}
			}
		}
	}

	// release a memory
	closedir(c_dir);
	

	// undeleted c_dir
	delete contents;

}


DirectoryHandler::~DirectoryHandler() {

	#ifdef RELEASE_DIRECTORY_DATA // release data about directory
		DirectoryFile* cur = this->startFiles;
		DirectoryFile* prev;
		while(cur != NULL) {
			prev = cur;
			cur = cur->next;

			//delete prev->body;
			delete prev;
		}
	#endif

}

size_t DirectoryHandler::count() {
	return this->_count;

}

DirectoryFile* DirectoryHandler::files() {
	return this->startFiles;
}
