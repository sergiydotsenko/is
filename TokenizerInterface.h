#pragma once

#include <string>

#include <codecvt>
#include <fcntl.h>
#include <io.h>

#include <fstream>
#include <streambuf>
#include <iostream>
#include <sstream>
#include <utility>

#include <algorithm>

#include <thread>
#include <mutex>

using namespace std;

#include "preprocessor.h"
#include "BookWordsStructure.h"


class TokenizerInterface {

protected:
	IRString fileAllocator;
	IRString body;
	BookWords* words;
	int countWords;
	IRString title;

public:
	TokenizerInterface(IRString filename):
		fileAllocator(filename),
		words(new BookWords()),
		body(""),
		countWords(0) {}



	virtual ~TokenizerInterface() {
		delete words;
	};

	virtual BookWords& tokens() = 0;

	inline IRString getTitle(){
		return this->title;
	}
	inline int size() {
		return this->countWords;
	}

protected:

	void readFileContents(int blockSize,void (*fileStreamHandler)(string& input,BookWordsItem* &bw,int lastWPosition)) {

		//vector<thread*> threads;
		ifstream i(this->fileAllocator);

		string out_s;
		
		int countlines = 0;


		this->words->start = new BookWordsItem(IRString(""),0);

		string linesBlock = string("");
		while(true) {

			getline(i, out_s);
			
			if(!i.eof()) {
				countlines++;
				linesBlock.append(out_s);
				if (countlines % blockSize == 0) {
					//thread* tempT = new thread(fileStreamHandler,linesBlock,ref(this->words->start),0);
					fileStreamHandler(linesBlock,this->words->start,0);
					//threads.push_back(tempT);
					linesBlock = string("");
				}
			} else {
					//thread* tempT = new thread(fileStreamHandler,linesBlock,ref(this->words->start),0);
					fileStreamHandler(linesBlock,this->words->start,0);
					//threads.push_back(tempT);
					break;
			}
		}
		
		/*for (size_t i = 0; i < threads.size(); i++){ 
				// waiting for threads being completed
				threads[i]->join();

				// free up memory used to store thread 
				delete threads[i];
		}*/
	}

};
