#pragma once

#include <map>
using namespace std;

#include "BookWordsStructure.h"
#include "BookWordsItemStructure.h"
#include "InvertedIndexWordBooks.h"


struct InvertedIndexWord
{

	bool isLast;
	map<char,InvertedIndexWord*>* next;
	InvertedIndexWordBooks* bookList;
	size_t qty;

	/* constructor */
	InvertedIndexWord():
		next(new map<char,InvertedIndexWord*>()),
		qty(0),
		bookList(NULL),
		isLast(false)
		//term("")
	{

	}

};
