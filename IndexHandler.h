#pragma once

#include <string>
#include <vector>
using namespace std;

#include "InvertedIndex.h"
#include "InvertedIndexWordBooks.h"
#include "IndexWordPosition.h"
#include "BookWordsStructure.h"

#include "StructuredFile.h"

class IndexHandler {

	InvertedIndex* index; 
	const short spatialSimilarityDistance;
	StructuredFile<unsigned int> diskOperator;

public:
	IndexHandler(InvertedIndex* const &item);
	~IndexHandler(void);

	// get word from contents database
	BookWordsItem* findWord(string &word);

	//get book by id
	void findBook(int id);
	void findBook(string title);

	// returns a result 
	void executeQuery(string q);

	//adds a book to an index
	void processBook(BookWords* const &words,int &bookId);


	void dump(string file);

private:

	// returns array of ids that was found in two lists
	InvertedIndexWordBooks* mergeBookLists(InvertedIndexWordBooks* smaller,InvertedIndexWordBooks* bigger);

	// return vector - offsets of coincedences in two merged lists
	int mergeSpatialLists(IndexWordPosition* smaller,IndexWordPosition* bigger,short distance);
	int mergeSpatialLists(IndexWordPosition* smaller,IndexWordPosition* bigger);

	// analyze query
	void analyzeQuery(string& q);

	// traverse thru the contents table 
	InvertedIndexWordBooks* traverseContents(string& searchWord);
	InvertedIndexWordBooks* checkRoot(InvertedIndexWord* const root, string& value);
};

