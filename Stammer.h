#pragma once

#ifndef _IS_STAMMER_
#define _IS_STAMMER_


#include <string>
using namespace std;

class Stammer
{
public:
	Stammer(void);
	~Stammer(void);


	inline string processWord(string& word);

};


#endif
