#pragma once

#include <string>
using namespace std;

#include "preprocessor.h"
#include "ModifiedString.h"

struct BookWordsItem {

	BookWordsItem* next;
	IRString value;
    unsigned int posInBook;

	BookWordsItem(IRString &word,int pos):
		next(NULL),
		value(word),
		posInBook(pos)
	{
		
	
	}

};
