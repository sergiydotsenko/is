#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
using namespace std;

#include "SearchUserInterface.h"

const int SearchUserInterface::QUERY_MAX_LEN = 255;

SearchUserInterface::SearchUserInterface(IndexHandler* const &item):
	hIndex(item)
{

}


SearchUserInterface::~SearchUserInterface(){

}


void SearchUserInterface::prompt(){
	
	char uQery[SearchUserInterface::QUERY_MAX_LEN];
	
	while  (uQery != "exit") {
		cout << "\n>"; 

		cin.ignore(); // waiting for user input
		cin.getline(uQery,QUERY_MAX_LEN); 
	
		//execute a query on a "database" handler
		this->hIndex->executeQuery(string(uQery));
	}
	

}
